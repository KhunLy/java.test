package tf.utils;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
// commentaire
public class Geometrie {
    public boolean isValidTriangle(String value) {
        String[] values = value.split(" ");
        List<Integer> valuesInt = new ArrayList<>();
        for(String v: values) {
            try {
                int nb = Integer.parseInt(v);
                valuesInt.add(nb);
                if (nb < 0) return false;
            } catch (Exception e) {
                return false;
            }
        }
        if(values.length != 3) return false;
        if(valuesInt.get(0) + valuesInt.get(1) < valuesInt.get(2))
            return false;
        if(valuesInt.get(0) + valuesInt.get(2) < valuesInt.get(1))
            return false;
        if(valuesInt.get(2) + valuesInt.get(1) < valuesInt.get(0))
            return false;

        return true;
    }
}
