package tf.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GeometrieTest {
    Geometrie geometrie = new Geometrie();
    @Test
    public void isValidTriangleTest() {
        //tester la chaine de caractères
        Assertions.assertFalse(
           geometrie.isValidTriangle("gfdsgdfmslhg"),
                "Les valeurs entrées ne sont pas correctes"
        );


        //tester max et min 3 nombres
        Assertions.assertFalse(
            geometrie.isValidTriangle("1 2")
        );
        Assertions.assertFalse(
            geometrie.isValidTriangle("1 2 3 4")
        );

        //tester valeurs > 0
        Assertions.assertFalse(
        geometrie.isValidTriangle("-1 1 1"));
        Assertions.assertFalse(
        geometrie.isValidTriangle("1 -1 1"));
        Assertions.assertFalse(
        geometrie.isValidTriangle("1 1 -1"));

        //tester la somme ...
        Assertions.assertFalse(
        geometrie.isValidTriangle("1 1 3"));
        Assertions.assertFalse(
        geometrie.isValidTriangle("3 1 1"));
        Assertions.assertFalse(
        geometrie.isValidTriangle("1 3 1"));

        //tester un cas fonctionnel
        Assertions.assertTrue(
        geometrie.isValidTriangle("3 3 3"));
    }
}