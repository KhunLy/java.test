package tf.utils;

import java.util.Arrays;

public class StringTransform {

    // v1
//    public String VerticalTransform(String originalValue){
//
//
//        String result = "";
//        for(char l: originalValue.toCharArray()){
//            result += l + "\n";
//        }
//        result = result.substring(0, result.length() - 1);
//        return result;
//    }

    public String VerticalTransform(String originalValue){

        String[] originalValues =
                Arrays.stream(originalValue.split(" "))
                        .filter( x -> !x.equals("") ).toArray(String[]::new);
        String result = "";
        for(int i=0; i < max(originalValues); i++){
            for(String word: originalValues){
                result += (word.length() > i) ? word.charAt(i) : " ";
            }
            result += "\n";
        }
        result = result.substring(0, result.length() - 1);
        return result;
    }

    private int max(String[] array) {
        int longest = 0;
        for(String w: array){
            if(w.length() > longest)
                longest = w.length();
        }
        return longest;
    }
}
