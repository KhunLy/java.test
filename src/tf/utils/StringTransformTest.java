package tf.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StringTransformTest {

    StringTransform tranformer  = new StringTransform();

    @Test
    void verticalTransformOneWord() {
        String val = tranformer.VerticalTransform("khun");
        Assertions.assertEquals("k\nh\nu\nn", val);
    }

    @Test
    void verticalTransformManyWordsSameSize() {
        String val = tranformer.VerticalTransform("papa papy");
        Assertions.assertEquals("pp\naa\npp\nay", val);
    }

    @Test
    void verticalTransformManyWordsDifferentSize() {
        String val = tranformer.VerticalTransform("papa papy maman");
        Assertions.assertEquals("ppm\naaa\nppm\naya\n  n", val);
    }

    @Test
    void verticalTransformManyWordsManySpaces() {
        String val = tranformer.VerticalTransform("papa   papy");
        Assertions.assertEquals("pp\naa\npp\nay", val);
    }

    @Test
    void verticalTransformSpecialChar() {
        String val = tranformer.VerticalTransform("papa p@py");
        Assertions.assertEquals("pp\na@\npp\nay", val);
    }

    @Test
    void verticalTransformCaseRespect() {
        String val = tranformer.VerticalTransform("paPa paPy");
        Assertions.assertEquals("pp\naa\nPP\nay", val);
    }
}